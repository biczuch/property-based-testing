module Tests

open Xunit
open FsCheck
open FsCheck.Xunit
open System.Text.RegularExpressions
open System
open FsCheck
open FsCheck
open FsCheck


let toCharArray (str:string) =
    str.ToCharArray()

let maskEmailGood (email:string) =
    let atSignIndex = email.IndexOf('@')

    let action index character = 
        match index with
        | i when i < atSignIndex -> '*'
        | _ -> character

    email |> String.mapi action
    
let maskEmailBad (email:string) =
    email |> String.map (fun c -> match c with
                                  | '@' -> c
                                  | _ -> '*')

let maskEmail = maskEmailBad

let removeSameCharsFromStart char str =
    let rec loop strArr =
        match strArr with
        | [] -> []
        | x::xs when x = char -> loop xs
        | arr -> arr
    
    let chars = str |> toCharArray |> List.ofArray
    let retVal = loop chars
    retVal |> System.String.Concat

type EmailArbitraries() =
    static member EmailGenerator() =
        let _emailGenRegex = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"
        let generator = gen {
            let xger = new Fare.Xeger(_emailGenRegex)
            return xger.Generate()
        }
        Arb.fromGen generator


[<Properties(
    MaxTest = 10,
    EndSize = 10,
    Verbose = true,
    QuietOnSuccess = false,
    Arbitrary=[| typeof<EmailArbitraries> |])>]
module EmailTests = 

    [<Property(Verbose = true)>]
    let ``Email is masked`` email =
        let rgx = new Regex("^\*+@.+\..+")
        let maskedEmail = email |> maskEmail
        rgx.Match(maskedEmail).Success

    [<Property>]
    let ``Masked email has same length`` email =
        let maskedEmail = email |> maskEmail
        (email |> String.length) = (maskedEmail |> String.length)


    [<Property>]
    let ``Masked email is not empty`` email =
        let maskedEmail = email |> maskEmail
        maskedEmail |> String.IsNullOrEmpty |> not


    [<Property>]
    let ``Length of asterisks is the same as address part of email`` email =
        let removeAsterisks = removeSameCharsFromStart '*'
        let maskedEmail = email |> maskEmail

        let maskedEmailWithoutAsterisks = maskedEmail |> removeAsterisks
        let asterisksLength = maskedEmail.Length - maskedEmailWithoutAsterisks.Length
        email.Substring(asterisksLength, 1) = "@" 
        && maskedEmailWithoutAsterisks.Substring(0,1) = "@"

    

    [<Property>]
    let ``Domain parts are the same for masked and email`` email =
        let removeAsterisks = removeSameCharsFromStart '*'
        let maskedEmail = email |> maskEmail

        let maskedEmailWithoutAsterisks = maskedEmail |> removeAsterisks
        let asterisksLength = maskedEmail.Length - maskedEmailWithoutAsterisks.Length
        email.Substring(asterisksLength) = maskedEmailWithoutAsterisks